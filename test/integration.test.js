// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', async t => {
	// eslint-disable-next-line unicorn/prefer-module
	const {verifyConditions, prepare} = require('../plugin.js');

	t.is(typeof verifyConditions, 'function');
	t.is(typeof prepare, 'function');

	const cfg = {};
	const ctx = {};

	await t.throwsAsync(verifyConditions(cfg, ctx));
	await t.throwsAsync(prepare(cfg, ctx));
});
