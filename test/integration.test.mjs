import {env} from 'node:process';
import {cp, mkdir} from 'node:fs/promises';
import {dirname, join} from 'node:path';
import test from 'ava';
import {stub} from 'sinon';
import clearModule from 'clear-module';
import {WritableStreamBuffer} from 'stream-buffers';
import {temporaryDirectory} from 'tempy';

test.beforeEach(async t => {
	clearModule('../plugin.mjs');
	t.context.m = await import('../plugin.mjs');

	t.context.cfg = {
		inputs: ['test/fixture/file.txt', 'test/fixture/folder'],
		archive: 'nested/archive.tar.gz',
	};

	const environ = Object.fromEntries(Object.entries(env));

	t.context.log = stub();
	t.context.ctx = {
		cwd: temporaryDirectory(),
		env: environ,
		options: {},
		stdout: new WritableStreamBuffer(),
		stderr: new WritableStreamBuffer(),
		logger: {
			log: t.context.log,
			success: t.context.log,
			warn: t.context.log,
		},
	};

	// Copy the test fixtures to the test's temporary directory
	await cp('test/fixture/', join(t.context.ctx.cwd, 'test/fixture/'), {recursive: true});
});

const failure = test.macro({
	// eslint-disable-next-line max-params
	async exec(t, code, regex, before, after) {
		if (before) {
			await before(t);
		}

		const error = await t.throwsAsync(
			t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
		);

		t.is(error.name, 'SemanticReleaseError', `${error}`);
		t.is(error.code, code, `${error}`);
		t.regex(error.message, regex, `${error}`);

		if (after) {
			await after(t);
		}
	},
	title: (message, code, regex) =>
		message ?? `Throws \`${code}\` matching ${regex.source}`,
});

const success = test.macro({
	async exec(t, before, after) {
		if (before) {
			await before(t);
		}

		await t.notThrowsAsync(
			t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
		);
		t.context.ctx.nextRelease = {version: '1.0.0'};
		await t.notThrowsAsync(t.context.m.prepare(t.context.cfg, t.context.ctx));

		if (after) {
			await after(t);
		}
	},
	title: message =>
		message ? `Successfully runs with ${message}` : 'Successfully runs',
});

test(failure, 'EARCHIVECFG', /`inputs` must be a list/, async t => {
	t.context.cfg.inputs = {};
});

test(failure, 'EARCHIVECFG', /`inputs` must be non-empty/, async t => {
	t.context.cfg.inputs = [];
});

test(failure, 'EARCHIVECFG', /Input paths do not exist: `.+`/, async t => {
	t.context.cfg.inputs = ['this/does/not/exist'];
});

test(failure, 'EARCHIVECFG', /`archive` is not a string/, async t => {
	t.context.cfg.archive = 0;
});

test(failure, 'EARCHIVECFG', /`archive` has unsupported extension/, async t => {
	t.context.cfg.archive = 'archive.undefined';
});

test(failure, 'EARCHIVECFG', /Cannot write to `archive` path/, async t => {
	const {cwd} = t.context.ctx;
	const {archive} = t.context.cfg;
	const parent = join(cwd, dirname(archive));

	// Use output directory with no write permissions
	await mkdir(parent, {mode: 0o555}); // 0o555 is r-xr-xr-x
});

test(failure, 'EARCHIVECFG', /`archive` is an absolute path/, async t => {
	t.context.cfg.archive = '/home/test/archive.tar.gz';
});

test(failure, 'EARCHIVECFG', /Input paths are absolute: `.+`/, async t => {
	t.context.cfg.inputs = ['/test/fixture/file.txt', 'test/fixture/folder'];
});

test(failure, 'EARCHIVECFG', /Failed to create output directory `.+`/, async t => {
	// Set output directory to be within an unwriteable parent directory
	t.context.cfg.archive = 'nested/parent/archive.tar.gz';
	const nested = join(t.context.ctx.cwd, 'nested/');
	await mkdir(nested, {mode: 0o555}); // 0o555 is r-xr-xr-x
});

test(success);
