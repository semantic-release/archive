# @semantic-release/archive

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> plugin to create archives.

| Step               | Description                             |
| ------------------ | --------------------------------------- |
| `verifyConditions` | Verify that the input paths exist.      |
| `prepare`          | Create an archive from the input paths. |

## Getting Started

```sh
npm install --save-dev https://gitlab.arm.com/semantic-release/archive.git
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - path: "@semantic-release/archive"
    inputs:
      - path1
      - path2
```

## Configuration

### `inputs`

The relative paths to add to the archive.

These can be directories and/or filepaths.

### `archive`

The relative path where the archive should be created.

Defaults to `archive.tar.gz`.
