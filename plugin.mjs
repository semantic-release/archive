import {lstatSync, createWriteStream} from 'node:fs';
import {access, writeFile, rm, mkdir} from 'node:fs/promises';
import {dirname, isAbsolute, join} from 'node:path';
import SemanticReleaseError from '@semantic-release/error';
import archiver from 'archiver';
import dbg from 'debug';

const debug = dbg('semantic-release:archive');

const DEFAULT_ARCHIVE_PATH = 'archive.tar.gz';
const COMPRESSION = {
	'.tar': ['tar', {gzip: false}],
	'.tar.gz': ['tar', {gzip: true, gzipOptions: {level: 9}}],
	'.tgz': ['tar', {gzip: true, gzipOptions: {level: 9}}],
	'.zip': ['zip', {zlib: {level: 9}}],
};

function verbatim(...args) {
	const backticks = '```';
	return args
		.map(a => `${backticks}\n${a}\n${backticks}`)
		.join('\n');
}

function extension(path) {
	const components = path.split('.');
	const [tar, ext] = components.slice(-2);

	if (components.length > 2 && tar === 'tar') {
		return `.${tar}.${ext}`;
	}

	return `.${ext}`;
}

export async function verifyConditions(pluginConfig, context) {
	const {logger, cwd} = context;
	const {inputs: relativeInputs, archive: relativeArchive = DEFAULT_ARCHIVE_PATH} = pluginConfig;

	debug('Validating input paths');

	// Make sure we have the correct shape for the paths
	if (!Array.isArray(relativeInputs)) {
		throw new SemanticReleaseError(
			'`inputs` must be a list',
			'EARCHIVECFG',
			verbatim(relativeInputs),
		);
	}

	if (relativeInputs.length === 0) {
		throw new SemanticReleaseError(
			'`inputs` must be non-empty',
			'EARCHIVECFG',
			verbatim(relativeInputs),
		);
	}

	// Make sure that the input paths are relative
	const absolute = relativeInputs.filter((_, i) => isAbsolute(relativeInputs[i]));
	if (absolute.length > 0) {
		throw new SemanticReleaseError(
			`Input paths are absolute: \`${absolute.join(', ')}\``,
			'EARCHIVECFG',
			verbatim(absolute),
		);
	}

	// Get the absolute input paths based on the working directory
	const inputs = relativeInputs.map(path => join(cwd, path));

	// Make sure that the input paths exist
	const exist = await Promise.allSettled(inputs.map(p => access(p)));
	const missing = inputs.filter((_, i) => exist[i].status === 'rejected');
	if (missing.length > 0) {
		throw new SemanticReleaseError(
			`Input paths do not exist: \`${missing.join(', ')}\``,
			'EARCHIVECFG',
			verbatim(missing),
		);
	}

	logger.success('Input paths are valid');
	debug('Validating output path');

	if (typeof relativeArchive !== 'string') {
		throw new SemanticReleaseError(
			'`archive` is not a string',
			'EARCHIVECFG',
			verbatim(relativeArchive),
		);
	}

	// Make sure that the archive output path isn't an absolute path
	if (isAbsolute(relativeArchive)) {
		throw new SemanticReleaseError(
			'`archive` is an absolute path',
			'EARCHIVECFG',
			verbatim(relativeArchive),
		);
	}

	const archive = join(cwd, relativeArchive);

	if (!Object.keys(COMPRESSION).includes(extension(archive))) {
		throw new SemanticReleaseError(
			'`archive` has unsupported extension',
			'EARCHIVECFG',
			`${extension(archive)} not in ${Object.keys(COMPRESSION).join(', ')}`,
		);
	}

	// Create the output directory if it doesn't already exist
	const directory = dirname(archive);
	try {
		await mkdir(directory, {recursive: true});
	} catch (error) {
		throw new SemanticReleaseError(
			`Failed to create output directory \`${directory}\``,
			'EARCHIVECFG',
			verbatim(error),
		);
	}

	// Make sure we have write permissions to the archive path
	try {
		await writeFile(archive, '');
		await rm(archive);
	} catch (error) {
		throw new SemanticReleaseError(
			`Cannot write to \`archive\` path: ${archive}`,
			'EARCHIVECFG',
			verbatim(error),
		);
	}

	logger.success('Output path is valid');
}

export async function prepare(pluginConfig, context) {
	const {logger, cwd} = context;
	const {nextRelease: {version}} = context;
	const {inputs: relativeInputs, archive: relativeArchive = DEFAULT_ARCHIVE_PATH} = pluginConfig;
	const archive = join(cwd, relativeArchive);
	const inputs = relativeInputs.map(path => join(cwd, path));

	await new Promise((resolve, reject) => {
		const archiveStream = archiver(...COMPRESSION[extension(archive)]);
		const output = createWriteStream(archive);
		output.on('close', resolve);
		archiveStream.on('warning', reject).on('error', reject).pipe(output);
		for (const path of inputs) {
			if (lstatSync(path).isDirectory()) {
				archiveStream.directory(path, path, {path});
			} else {
				archiveStream.file(path, {path});
			}
		}

		archiveStream.finalize();
	});

	logger.success('Created an archive for `%s`', version);
}
